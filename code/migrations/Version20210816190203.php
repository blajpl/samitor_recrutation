<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210816190203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, guid VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, link VARCHAR(255) NOT NULL, image_link VARCHAR(255) NOT NULL, `condition` VARCHAR(255) NOT NULL, availability VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, shipping_country VARCHAR(255) NOT NULL, shipping_service VARCHAR(255) NOT NULL, shipping_price DOUBLE PRECISION NOT NULL, gtin INT DEFAULT NULL, brand VARCHAR(255) DEFAULT NULL, mpn VARCHAR(255) DEFAULT NULL, google_product_category VARCHAR(255) DEFAULT NULL, product_type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE product');
    }
}
