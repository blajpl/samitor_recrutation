<?php

namespace App\Service;

use App\Dto\ProductDto;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class ProductService
{
    private $productRepository;

    private $entityManager;

    public function __construct(
        ProductRepository      $productRepository,
        EntityManagerInterface $entityManager)
    {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param ProductDto[] $productsDtoArray
     */
    public function saveProducts(array $productsDtoArray): void
    {
        foreach ($productsDtoArray as $productDto) {
            $product = (new Product())
                ->setGuid($productDto->getGuid())
                ->setTitle($productDto->getTitle())
                ->setDescription($productDto->getDescription())
                ->setLink($productDto->getLink())
                ->setImageLink($productDto->getImageLink())
                ->setCondition($productDto->getCondition())
                ->setAvailability($productDto->getAvailability())
                ->setPrice($productDto->getPrice())
                ->setShippingCountry($productDto->getShippingCountry())
                ->setShippingService($productDto->getShippingService())
                ->setShippingPrice($productDto->getShippingPrice())
                ->setGtin($productDto->getGtin())
                ->setBrand($productDto->getBrand())
                ->setMpn($productDto->getMpn())
                ->setGoogleProductCategory($productDto->getGoogleProductCategory())
                ->setProductType($productDto->getProductType());

            $this->entityManager->persist($product);
        }

        $this->entityManager->flush();
    }

    public function create(Product $product)
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    public function update($id, Product $product)
    {
        $existingProduct = $this->fetchProduct($id);

        $existingProduct->setGuid($product->getGuid())
            ->setTitle($product->getTitle())
            ->setDescription($product->getDescription())
            ->setLink($product->getLink())
            ->setImageLink($product->getImageLink())
            ->setCondition($product->getCondition())
            ->setAvailability($product->getAvailability())
            ->setShippingCountry($product->getShippingCountry())
            ->setShippingService($product->getShippingService())
            ->setShippingPrice($product->getShippingPrice())
            ->setGtin($product->getGtin())
            ->setMpn($product->getMpn())
            ->setGoogleProductCategory($product->getGoogleProductCategory())
            ->setProductType($product->getProductType());

        $this->entityManager->persist($existingProduct);
        $this->entityManager->flush();
    }

    public function delete($id)
    {
        $product = $this->fetchProduct($id);

        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }

    /**
     * @throws EntityNotFoundException
     */
    public function fetchProduct($id): ?Product
    {
        $product = $this->productRepository->findOneBy(['id' => $id]);

        if (!$product) {
            throw new EntityNotFoundException();
        }

        return $product;
    }
}