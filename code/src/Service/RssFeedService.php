<?php

namespace App\Service;

use App\Mapper\ProductDtoMapper;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class RssFeedService {

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function loadProducts(UploadedFile $uploadedFile) {
        $xml =
            simplexml_load_string(
                preg_replace(
                    '/&(?!#?[a-z0-9]+;)/',
                    '&amp;',
                    file_get_contents($uploadedFile)));
        $xml->registerXPathNamespace('g', 'http://base.google.com/ns/1.0');

        $productsDtoArray = [];
        foreach ($xml->channel->item as $itemProduct) {
            $mapped = ProductDtoMapper::map($itemProduct);

            if (!$mapped) {
                continue;
            }

            $productsDtoArray[] = $mapped;
        }

        $this->productService->saveProducts($productsDtoArray);
    }
}