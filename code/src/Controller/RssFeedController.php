<?php

namespace App\Controller;

use App\Form\RssProductType;
use App\Service\RssFeedService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rssfeed")
 */
class RssFeedController extends AbstractController {

    private $rssFeedService;

    public function __construct(RssFeedService $rssFeedService)
    {
        $this->rssFeedService = $rssFeedService;
    }

    /**
     * @Route("/load", methods={"GET", "POST"})
     */
    public function load(Request $request): Response {
        $form = $this->createForm(RssProductType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();

            $this->rssFeedService->loadProducts($file);
        }

        return $this->render('rss_feed/load.html.twig', [
            'form' => $form->createView()
        ]);
    }
}