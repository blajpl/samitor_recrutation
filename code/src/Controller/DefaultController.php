<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/")
 */
class DefaultController
{

    /**
     * @Route("/", methods={"GET"})
     */
    public function index(): Response
    {
        return new Response();
    }
}