<?php

namespace App\Controller;

use App\Dto\ProductCreateRequest;
use App\Dto\ProductUpdateRequest;
use App\Message\ProductCreateMessage;
use App\Message\ProductDeleteMessage;
use App\Message\ProductUpdateMessage;
use App\Service\ProductService;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ProductController extends AbstractController
{

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @Route("/api/product", methods={"POST"})
     */
    public function create(
        ProductCreateRequest             $productCreateRequest,
        ConstraintViolationListInterface $errors): JsonResponse
    {
        if ($errors->count() > 0) {
            dd($errors);
            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        $productCreateMessage = (new ProductCreateMessage())
            ->setGuid($productCreateRequest->guid)
            ->setTitle($productCreateRequest->title)
            ->setDescription($productCreateRequest->description)
            ->setLink($productCreateRequest->link)
            ->setImageLink($productCreateRequest->imageLink)
            ->setCondition($productCreateRequest->condition)
            ->setAvailability($productCreateRequest->availability)
            ->setPrice($productCreateRequest->price)
            ->setShippingCountry($productCreateRequest->shippingCountry)
            ->setShippingService($productCreateRequest->shippingService)
            ->setShippingPrice($productCreateRequest->shippingPrice)
            ->setGtin($productCreateRequest->gtin)
            ->setBrand($productCreateRequest->brand)
            ->setMpn($productCreateRequest->mpn)
            ->setGoogleProductCategory($productCreateRequest->googleProductCategory)
            ->setProductType($productCreateRequest->productType);

        $this->dispatchMessage($productCreateMessage);

        return new JsonResponse('', Response::HTTP_CREATED);
    }

    /**
     * @Route("/api/product/{id}", methods={"PUT"})
     */
    public function update(
        int                              $id,
        ProductUpdateRequest             $productUpdateRequest,
        ConstraintViolationListInterface $errors): JsonResponse
    {
        if ($errors->count() > 0) {
            return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
        }

        $product = $this->productService->fetchProduct($id);

        $productUpdateMessage = (new ProductUpdateMessage())
            ->setId($product->getId())
            ->setGuid($productUpdateRequest->guid)
            ->setTitle($productUpdateRequest->title)
            ->setDescription($productUpdateRequest->description)
            ->setLink($productUpdateRequest->link)
            ->setImageLink($productUpdateRequest->imageLink)
            ->setCondition($productUpdateRequest->condition)
            ->setAvailability($productUpdateRequest->availability)
            ->setPrice($productUpdateRequest->price)
            ->setShippingCountry($productUpdateRequest->shippingCountry)
            ->setShippingService($productUpdateRequest->shippingService)
            ->setShippingPrice($productUpdateRequest->shippingPrice)
            ->setGtin($productUpdateRequest->gtin)
            ->setBrand($productUpdateRequest->brand)
            ->setMpn($productUpdateRequest->mpn)
            ->setGoogleProductCategory($productUpdateRequest->googleProductCategory)
            ->setProductType($productUpdateRequest->productType);

        $this->dispatchMessage($productUpdateMessage);

        return new JsonResponse('', Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/api/product/{id}", methods={"DELETE"})
     */
    public function delete(int $id): JsonResponse
    {
        $product = $this->productService->fetchProduct($id);

        $productDeleteMessage = (new ProductDeleteMessage())
            ->setId($product->getId());

        $this->dispatchMessage($productDeleteMessage);

        return new JsonResponse('', Response::HTTP_ACCEPTED);
    }
}