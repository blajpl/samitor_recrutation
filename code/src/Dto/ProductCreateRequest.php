<?php

namespace App\Dto;

use Nelexa\RequestDtoBundle\Dto\RequestBodyObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ProductCreateRequest implements RequestBodyObjectInterface
{

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $guid;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $title;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $description;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $link;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $imageLink;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $condition;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $availability;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?float $price;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $shippingCountry;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $shippingService;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    public ?string $shippingPrice;

    public ?int $gtin;

    public ?string $brand;

    public ?string $mpn;

    public ?string $googleProductCategory;

    public ?string $productType;
}