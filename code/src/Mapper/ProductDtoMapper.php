<?php

namespace App\Mapper;

use App\Dto\ProductDto;
use SimpleXMLElement;

final class ProductDtoMapper
{

    public static function map(?SimpleXMLElement $xmlElement): ?ProductDto
    {
        if (!$xmlElement) {
            return null;
        }

        return (new ProductDto())
            ->setGuid(trim($xmlElement->xpath('//g:id')[0]))
            ->setTitle(trim($xmlElement->xpath('//g:title')[0]))
            ->setDescription(trim($xmlElement->xpath('//g:description')[0]))
            ->setLink(trim($xmlElement->xpath('//g:link')[0]))
            ->setImageLink(trim($xmlElement->xpath('//g:image_link')[0]))
            ->setCondition(trim($xmlElement->xpath('//g:condition')[0]))
            ->setAvailability(trim($xmlElement->xpath('//g:availability')[0]))
            ->setPrice((float)trim($xmlElement->xpath('//g:price')[0]))
            ->setShippingCountry(trim($xmlElement->xpath('//g:shipping')[0]->xpath('//g:country')[0]))
            ->setShippingService(trim($xmlElement->xpath('//g:price')[0]->xpath('//g:service')[0]))
            ->setShippingPrice((float) trim($xmlElement->xpath('//g:price')[0]->xpath('//g:price')[0]))
            ->setGtin(self::nonRequiredField($xmlElement, 'gtin'))
            ->setBrand(self::nonRequiredField($xmlElement, 'brand'))
            ->setMpn(self::nonRequiredField($xmlElement, 'mpn'))
            ->setGoogleProductCategory(trim($xmlElement->xpath('//g:google_product_category')[0]))
            ->setProductType(trim($xmlElement->xpath('//g:product_type')[0]));
    }

    private static function nonRequiredField(SimpleXMLElement $xmlElement, $fieldName): ?string
    {
        $value = $xmlElement->xpath('//g:' . $fieldName);

        if (count($value) == 0) {
            return null;
        }

        return trim($value[0]);
    }
}