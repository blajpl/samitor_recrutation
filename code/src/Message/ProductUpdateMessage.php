<?php

namespace App\Message;

class ProductUpdateMessage
{
    private int $id;

    private string $guid;

    private string $title;

    private string $description;

    private string $link;

    private string $imageLink;

    private string $condition;

    private string $availability;

    private float $price;

    private string $shippingCountry;

    private string $shippingService;

    private string $shippingPrice;

    private int $gtin;

    private string $brand;

    private string $mpn;

    private string $googleProductCategory;

    private string $productType;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ProductUpdateMessage
    {
        $this->id = $id;
        return $this;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;
        return $this;
    }

    public function getImageLink(): string
    {
        return $this->imageLink;
    }

    public function setImageLink(string $imageLink): self
    {
        $this->imageLink = $imageLink;
        return $this;
    }

    public function getCondition(): string
    {
        return $this->condition;
    }

    public function setCondition(string $condition): self
    {
        $this->condition = $condition;
        return $this;
    }

    public function getAvailability(): string
    {
        return $this->availability;
    }

    public function setAvailability(string $availability): self
    {
        $this->availability = $availability;
        return $this;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getShippingCountry(): string
    {
        return $this->shippingCountry;
    }

    public function setShippingCountry(string $shippingCountry): self
    {
        $this->shippingCountry = $shippingCountry;
        return $this;
    }

    public function getShippingService(): string
    {
        return $this->shippingService;
    }

    public function setShippingService(string $shippingService): self
    {
        $this->shippingService = $shippingService;
        return $this;
    }

    public function getShippingPrice(): string
    {
        return $this->shippingPrice;
    }

    public function setShippingPrice(string $shippingPrice): self
    {
        $this->shippingPrice = $shippingPrice;
        return $this;
    }

    public function getGtin(): int
    {
        return $this->gtin;
    }

    public function setGtin(int $gtin): self
    {
        $this->gtin = $gtin;
        return $this;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;
        return $this;
    }

    public function getMpn(): string
    {
        return $this->mpn;
    }

    public function setMpn(string $mpn): self
    {
        $this->mpn = $mpn;
        return $this;
    }

    public function getGoogleProductCategory(): string
    {
        return $this->googleProductCategory;
    }

    public function setGoogleProductCategory(string $googleProductCategory): self
    {
        $this->googleProductCategory = $googleProductCategory;
        return $this;
    }

    public function getProductType(): string
    {
        return $this->productType;
    }

    public function setProductType(string $productType): self
    {
        $this->productType = $productType;
        return $this;
    }
}