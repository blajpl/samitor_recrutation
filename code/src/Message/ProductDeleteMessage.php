<?php

namespace App\Message;

class ProductDeleteMessage
{
    private int $id;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ProductDeleteMessage
    {
        $this->id = $id;
        return $this;
    }
}