<?php

namespace App\MessageHandler;

use App\Entity\Product;
use App\Message\ProductCreateMessage;
use App\Service\ProductService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductCreateMessageHandler implements MessageHandlerInterface
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function __invoke(ProductCreateMessage $productCreateMessage)
    {
        $product = (new Product())
            ->setGuid($productCreateMessage->getGuid())
            ->setTitle($productCreateMessage->getTitle())
            ->setDescription($productCreateMessage->getDescription())
            ->setLink($productCreateMessage->getLink())
            ->setImageLink($productCreateMessage->getImageLink())
            ->setCondition($productCreateMessage->getCondition())
            ->setAvailability($productCreateMessage->getAvailability())
            ->setShippingCountry($productCreateMessage->getShippingCountry())
            ->setShippingService($productCreateMessage->getShippingService())
            ->setShippingPrice($productCreateMessage->getShippingPrice())
            ->setGtin($productCreateMessage->getGtin())
            ->setMpn($productCreateMessage->getMpn())
            ->setGoogleProductCategory($productCreateMessage->getGoogleProductCategory())
            ->setProductType($productCreateMessage->getProductType());

        $this->productService->create($product);
    }
}