<?php

namespace App\MessageHandler;

use App\Entity\Product;
use App\Message\ProductDeleteMessage;
use App\Service\ProductService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductDeleteMessageHandler implements MessageHandlerInterface
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function __invoke(ProductDeleteMessage $productDeleteMessage)
    {
        $this->productService->delete($productDeleteMessage->getId());
    }
}