<?php

namespace App\MessageHandler;

use App\Entity\Product;
use App\Message\ProductUpdateMessage;
use App\Service\ProductService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductUpdateMessageHandler implements MessageHandlerInterface
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function __invoke(ProductUpdateMessage $productUpdateMessage)
    {
        $product = (new Product())
            ->setGuid($productUpdateMessage->getGuid())
            ->setTitle($productUpdateMessage->getTitle())
            ->setDescription($productUpdateMessage->getDescription())
            ->setLink($productUpdateMessage->getLink())
            ->setImageLink($productUpdateMessage->getImageLink())
            ->setCondition($productUpdateMessage->getCondition())
            ->setAvailability($productUpdateMessage->getAvailability())
            ->setShippingCountry($productUpdateMessage->getShippingCountry())
            ->setShippingService($productUpdateMessage->getShippingService())
            ->setShippingPrice($productUpdateMessage->getShippingPrice())
            ->setGtin($productUpdateMessage->getGtin())
            ->setMpn($productUpdateMessage->getMpn())
            ->setGoogleProductCategory($productUpdateMessage->getGoogleProductCategory())
            ->setProductType($productUpdateMessage->getProductType());

        $this->productService->update($productUpdateMessage->getId(), $product);
    }
}